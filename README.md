Welcome to Infinity UX Design Studio README 

The project is mainly about a �UX Design Studio� which offers different services.
The UI contains several pages include: 

	-Home Page
	-Who we are
	-What we do
	-Activity
	-Feedback
	-Direct Email link
	
Some of the pages are under construction which you can find the names in issues file, 
please add any other issues you recommend fixing.

Prerequisites:
You will need to download the files into your local and to check the output, you have to open 
index.html with any browsers you prefer.
All the CSS files are under styles folder.
There is a Template.html which you can use as a template for your own project.
You need to have all the files in the same folder to be able to have all the content and images 
properly.

Technical Information:
The languages used are HTML and CSS.
To edit any file codes, you need to open the file with any text editor, my favorite is: Brackets.

Additional Information:
Please read the License.txt file with the Licenses and Standars for the open source initiative.
You may check the issues and find underconstruction.html file's name which I need to add a video on it.

Shima Maleki--
